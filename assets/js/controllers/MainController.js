app.controller("MainController", ["$scope","$http",function($scope,$http){
    $scope.items = new Array(100);
    $scope.list_view = null;
    $scope.max = null;

    $scope.findClass = function(num){
        if(num == 4  || (num- 4).mod(12) == 0) return 'item-left';
        if(num == 11 || (num-11).mod(12) == 0) return 'item-right';
    };

    $scope.open = function(index){
        if($scope.list_view) return true;
        $scope.list_view = true;

        setTimeout(function(){
            document.getElementById('item-'+index).scrollIntoView();
        },0);
    };

    var options = {
        root: null,
        rootMargin: '0px',
        threshold: [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    };

    var visibles = {};
    var callback = function(entries, observer) { 
        entries.forEach(function(entry){
            let visiblePct = (Math.floor(entry.intersectionRatio * 100));
            var id = entry.target.id.split('-').pop();
            visibles[id] = visiblePct;

            if(visiblePct == 0)
                delete visibles[id];

            for(var key in visibles)
                if(visibles[key] > 50)
                    $scope.max = key;
        });
    };

    var observer = new IntersectionObserver(callback, options);
    

    setTimeout(function(){
        var items = document.querySelectorAll('.item');
        [...items].map(function(target){
            observer.observe(target);
        });

    },100);

    $scope.$watch("list_view", function(nextVal, prevVal){
        if(prevVal === true && nextVal === false){
            setTimeout(function(){
                document.getElementById('item-'+$scope.max).scrollIntoView();
            }, 0);
        }
    }, true);

}]);