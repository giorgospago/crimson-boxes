var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var minify = require('gulp-minify');
var minifycss = require('gulp-minify-css');
var sourcemaps = require('gulp-sourcemaps');

gulp.task("sass", function(){
    gulp
        .src('./assets/scss/main.scss')
        .pipe(concat('style.scss'))
        .pipe(sass())
        .pipe(gulp.dest('./assets/css'));
});

gulp.task('watch', function () {
    gulp.watch('./assets/scss/*.scss', ['sass']);
});