app.controller("MainController", ["$scope","$http",function($scope,$http){
    $scope.items = new Array(23);

    var left_start = 4;
    var right_start = 11;
    var count = 0;

    $scope.findClass = function(num){
        var myClass = "";

        if(num == left_start){
            left_start = left_start + 12;
            myClass = 'item-left';
        }

        if(num == right_start){
            right_start = right_start + 12;
            myClass = 'item-right';
        }

        count++;
        if(count == $scope.items.length){
            left_start = 4;
            right_start = 11;
        }

        return myClass;
    };

}]);